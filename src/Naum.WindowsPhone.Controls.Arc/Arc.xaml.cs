﻿namespace Naum.WindowsPhone.Controls
{
	using System;
	using System.ComponentModel;
	using System.Diagnostics;
	using System.Globalization;
	using System.Reflection;
	using System.Windows;
	using System.Windows.Controls;
	using System.Windows.Media;
	using Microsoft.Phone.Controls;

	public partial class Arc : UserControl, INotifyPropertyChanged
	{
		#region Fields
		public static DependencyProperty FillProperty =
			DependencyProperty.Register("Fill", typeof(Brush), typeof(Arc), new PropertyMetadata(new SolidColorBrush(Colors.Blue)));
		public static DependencyProperty StartAngleProperty =
			DependencyProperty.Register("StartAngle", typeof(double), typeof(Arc), new PropertyMetadata(1.0, StartAnglePropertyChangedCallback));
		public static DependencyProperty EndAngleProperty =
			DependencyProperty.Register("EndAngle", typeof(double), typeof(Arc), new PropertyMetadata(2.0, EndAnglePropertyChangedCallback));
		private Point _center;
		private Point _start;
		private Size _size;
		private bool _isLargeArc;
		private Point _end;
		private double _radius;
		#endregion

		#region Events
		public event PropertyChangedEventHandler PropertyChanged;
		#endregion

		#region Constructors
		/// <summary>
		/// Initializes a new instance of the <see cref="Arc"/> class.
		/// </summary>
		public Arc()
		{
			InitializeComponent();
			LayoutRoot.DataContext = this;
		}
		#endregion

		#region Properties
		/// <summary>
		/// Gets or sets the fill.
		/// </summary>
		/// <value>
		/// The fill.
		/// </value>
		public Brush Fill
		{
			get
			{
				return (Brush)GetValue(FillProperty);
			}

			set
			{
				SetValue(FillProperty, value);
			}
		}

		/// <summary>
		/// Gets or sets the start angle.
		/// </summary>
		/// <value>
		/// The start angle.
		/// </value>
		public double StartAngle
		{
			get
			{
				return (double)GetValue(StartAngleProperty);
			}

			set
			{
				SetValue(StartAngleProperty, value);
			}
		}

		/// <summary>
		/// Gets or sets the end angle.
		/// </summary>
		/// <value>
		/// The end angle.
		/// </value>
		public double EndAngle
		{
			get
			{
				return (double)GetValue(EndAngleProperty);
			}

			set
			{
				SetValue(EndAngleProperty, value);
			}
		}

		/// <summary>
		/// Gets or sets the center.
		/// </summary>
		/// <value>
		/// The center.
		/// </value>
		public Point Center
		{
			get
			{
				return _center;
			}

			set
			{
				_center = value;
				OnPropertyChanged(new PropertyChangedEventArgs("Center"));
			}
		}

		/// <summary>
		/// Gets or sets the start.
		/// </summary>
		/// <value>
		/// The start.
		/// </value>
		public Point Start
		{
			get
			{
				return _start;
			}

			set
			{
				_start = value;
				OnPropertyChanged(new PropertyChangedEventArgs("Start"));
			}
		}

		/// <summary>
		/// Gets or sets the size.
		/// </summary>
		/// <value>
		/// The size.
		/// </value>
		public Size Size
		{
			get
			{
				return _size;
			}

			set
			{
				_size = value;
				OnPropertyChanged(new PropertyChangedEventArgs("Size"));
			}
		}

		/// <summary>
		/// Gets or sets a value indicating whether this arc is large arc.
		/// </summary>
		/// <value>
		///   <c>true</c> if this arc is large arc] otherwise, <c>false</c>.
		/// </value>
		public bool IsLargeArc
		{
			get
			{
				return _isLargeArc;
			}

			set
			{
				_isLargeArc = value;
				OnPropertyChanged(new PropertyChangedEventArgs("IsLargeArc"));
			}
		}

		/// <summary>
		/// Gets or sets the end.
		/// </summary>
		/// <value>
		/// The end.
		/// </value>
		public Point End
		{
			get
			{
				return _end;
			}

			set
			{
				_end = value;
				OnPropertyChanged(new PropertyChangedEventArgs("End"));
			}
		}
		#endregion

		#region Methods
		/// <summary>
		/// Provides the behavior for the Arrange pass of Silverlight layout. Classes can override this method to define their own Arrange pass behavior.
		/// </summary>
		/// <param name="finalSize">The final area within the parent that this object should use to arrange itself and its children.</param>
		/// <returns>
		/// The actual size that is used after the element is arranged in layout.
		/// </returns>
		protected override Size ArrangeOverride(Size finalSize)
		{
			Size result = base.ArrangeOverride(finalSize);
			CalculateRadius(result);
			CalculateCenter();
			return result;
		}

		/// <summary>
		/// Raises the <see cref="E:PropertyChanged" /> event.
		/// </summary>
		/// <param name="e">The <see cref="PropertyChangedEventArgs"/> instance containing the event data.</param>
		protected virtual void OnPropertyChanged(PropertyChangedEventArgs e)
		{
			CheckPropertyName(e.PropertyName);
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, e);
			}
		}

		/// <summary>
		/// Callback, which is raised, when StartAngle property is changed..
		/// </summary>
		/// <param name="d">The object, which property is changed.</param>
		/// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
		private static void StartAnglePropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			Arc arc = (Arc)d;
			arc.CalculateStart();
			arc.CalculateIsLargeArc();
		}

		/// <summary>
		/// Callback, which is raised, when EndAngle property is changed.
		/// </summary>
		/// <param name="d">The object, which property is changed.</param>
		/// <param name="e">The <see cref="DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
		private static void EndAnglePropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			Arc arc = (Arc)d;
			arc.CalculateEnd();
			arc.CalculateIsLargeArc();
		}

		/// <summary>
		/// Calculates the radius.
		/// </summary>
		/// <param name="size">The size.</param>
		private void CalculateRadius(Size size)
		{
			_radius = size.Width / 2;
			CalculateSize();
		}

		/// <summary>
		/// Calculates the size.
		/// </summary>
		private void CalculateSize()
		{
			Size = new Size(_radius * 2, _radius * 2);
		}

		/// <summary>
		/// Calculates the center.
		/// </summary>
		private void CalculateCenter()
		{
			GeneralTransform transform = TransformToVisual(((PhoneApplicationFrame)Application.Current.RootVisual).Content as UIElement);
			Point topLeftOffset = transform.Transform(new Point(0, 0));
			Center = new Point(topLeftOffset.X + ActualWidth / 2, topLeftOffset.Y + ActualHeight / 2);
			CalculateStart();
			CalculateEnd();
		}

		/// <summary>
		/// Calculates the start.
		/// </summary>
		private void CalculateStart()
		{
			Start = new Point(Center.X + _radius * Math.Cos(StartAngle), Center.Y + _radius * Math.Sin(StartAngle));
		}

		/// <summary>
		/// Calculates the end.
		/// </summary>
		private void CalculateEnd()
		{
			End = new Point(Center.X + _radius * Math.Cos(EndAngle), Center.Y + _radius * Math.Sin(EndAngle));
		}

		/// <summary>
		/// Calculates whether this arc is a large arc.
		/// </summary>
		private void CalculateIsLargeArc()
		{
			IsLargeArc = (EndAngle - StartAngle) > Math.PI;
		}

		/// <summary>
		/// Checks the name of the property.
		/// </summary>
		/// <param name="propertyName">Name of the property.</param>
		[Conditional("DEBUG")]
		private static void CheckPropertyName(string propertyName)
		{
			Type thisType = typeof(Arc);
			PropertyInfo[] properties = thisType.GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Static);
			bool isPropertyNameCorrect = false;
			foreach (PropertyInfo property in properties)
			{
				if (property.Name.Equals(propertyName, StringComparison.Ordinal))
				{
					isPropertyNameCorrect = true;
					break;
				}
			}
			if (!isPropertyNameCorrect)
			{
				throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture,
																	"The type '{0}' does not contain the property with name '{1}'",
																	typeof(Arc).FullName,
																	propertyName));

			}
		}
		#endregion
	}
}
